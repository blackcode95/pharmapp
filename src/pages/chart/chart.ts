import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Location } from '@angular/common';
import { AngularFireDatabase } from '@angular/fire/database';

/**
 * Generated class for the ChartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
})
export class ChartPage {

 salesArray = [];
 slideOpts = {
  effect: 'flip'
};
  chart = {
    title: '',
    type: 'BarChart',
    data: [
      ['MOYENNE', 700],
      ['BE-PHARMA', 500],
    ],
    columnNames: ['Element', 'Sales'],
    options: {
      animation: {
        duration: 250,
        easing: 'ease-in-out',
        startup: true
      }
    }
  };

  chart1 = {
    title: '',
    type: 'BarChart',
    data: [
      ['Paracetamol', 206],
      ['Aspirine', 105],
      ['Morphine', 190],
      ['Dalfagan', 150],
    ],
    columnNames: ['Element', 'Drugs Sold'],
    options: {
      animation: {
        duration: 250,
        easing: 'ease-in-out',
        startup: true
      }
    }
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private afDb: AngularFireDatabase) {

    this.afDb.list('/sale').valueChanges().subscribe(res=>{
      this.salesArray = res;

      console.log(res);
    });



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChartPage');
  }


}
