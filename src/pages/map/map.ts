import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-map",
  templateUrl: "map.html"
})
export class MapPage {
  height = 400;
  width = 400;
  maxDataPoints = 500;
  minDataPoints = 500;
  heatPoints = {};

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad MapPage");
  }
}
