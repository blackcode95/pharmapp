import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs-compat';
import { DatePipe } from '@angular/common';
import { HttpModule, Http } from '@angular/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  drugs = [];
  pharmacyArray = [];
  currentPharmacy : any; 

  constructor(public navCtrl: NavController, private afDb : AngularFireDatabase, private http: Http, private datepipe: DatePipe) {

  // this.drugs = 
  this.afDb.list('/drug').valueChanges().subscribe(res=>{
    this.drugs = res;
    console.log(res);
  });

  this.afDb.list('/pharmacy').valueChanges().subscribe(res=>{
    this.pharmacyArray = res;
    console.log(res);
  });

  }



  sell(_drug){
    let pharm : any;

    this.pharmacyArray.forEach(res=>{
      if(res.name == this.currentPharmacy){
        console.log("current pharmacy", this.currentPharmacy);
        pharm = res;
      }
    })
    console.log(pharm);
    let myDate = new Date();
    let dateString = this.datepipe.transform(myDate, "yyyy-MM-dd");
    console.log(dateString);
    let latitude = pharm.latitude;
    console.log(latitude);
    let longitude = pharm.longitude;
    console.log(longitude);
    let drugname = _drug.name;
    console.log(drugname);
    let illness = _drug.illness;
    console.log(illness);
    console.log(pharm.id);

    // let object = {date : dateString, idPharmacy: pharm.id, drug: drugname, illness: illness, latitude: latitude, longitude: longitude};

    // this.afDb.list("sale").push(object);


        fetch(
          "http://192.168.80.242/addSale?date="+dateString+"&idPharmacy="+pharm.id+"&drug="+drugname+"&illness="+illness+"&latitude="+latitude+"&longitude="+longitude

        );

  }


  getPharmacy(){
    
    return;
  }

}
