import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";

import { HomePage } from "../pages/home/home";
import { TabsPage } from "../pages/tabs/tabs";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AngularFireModule } from "@angular/fire";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase
} from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { DatePipe } from "@angular/common";
import { GoogleChartsModule } from 'angular-google-charts';
import { ChartPage } from "../pages/chart/chart";
import { MapPage } from "../pages/map/map";
import { HttpModule } from '@angular/http';


export const firebaseConfig = {
  apiKey: "AIzaSyCUbX-aV6MtXnF4Mya-rlkIuVjrlHSIkMg",
  authDomain: "hackaton-2c024.firebaseapp.com",
  databaseURL: "https://hackaton-2c024.firebaseio.com",
  projectId: "hackaton-2c024",
  storageBucket: "hackaton-2c024.appspot.com",
  messagingSenderId: "717244714739"
};

@NgModule({
  declarations: [MyApp, HomePage, TabsPage, ChartPage, MapPage],
  imports: [BrowserModule,
     IonicModule.forRoot(MyApp),
     GoogleChartsModule.forRoot(),
     AngularFireModule.initializeApp(firebaseConfig),
     AngularFireDatabaseModule,
     HttpModule,
     AngularFireAuthModule],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage, TabsPage, ChartPage, MapPage],
  providers: [
    StatusBar,
    DatePipe,
    AngularFireDatabase,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
